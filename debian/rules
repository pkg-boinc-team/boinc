#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Uncomment the following lines to compile with gcc-snapshot.
#export  CC=/usr/lib/gcc-snapshot/bin/gcc
#export CXX=/usr/lib/gcc-snapshot/bin/g++
#export CPP=/usr/lib/gcc-snapshot/bin/cpp

export CPPFLAGS := $(shell dpkg-buildflags --get CPPFLAGS)
export CFLAGS   := $(shell dpkg-buildflags --get CFLAGS)
export CXXFLAGS := $(shell dpkg-buildflags --get CXXFLAGS)
export LDFLAGS  := $(shell dpkg-buildflags --get LDFLAGS)

# BOINC client itself is not time-critical and may be an older machine with no AVX yet
#ARCH_AVX := i386 amd64 x32 hurd-i386
#ifneq (,$(findstring $(DEB_HOST_ARCH),$(ARCH_AVX)))
#  CPPFLAGS += -mavx
#  CXXFLAGS += -mavx
#endif

#export CXX=clang++
export AR=ar
export RANLIB=ranlib

DEB_HOST_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

# Set default flags with dpkg-buildflags
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
  TYPE_FLAGS = --build $(DEB_HOST_GNU_TYPE)
else
  TYPE_FLAGS = --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
endif

# On some architectures it is required to change the BOINC platform of the
# core client (with the --with-boinc-platform configure option) to match the
# official BOINC platform, otherwise it would not download any applications,
# because projects mostly provide applications for official BOINC platforms
# only. See https://boinc.berkeley.edu/trac/wiki/BoincPlatforms for the list
# of official BOINC platforms.
#
# The table of architectures known to dpkg-architecture are listed on
#  /usr/share/dpkg/archtable

# Change BOINC platform alpha-unknown-linux-gnu to alpha-hp-linux-gnu and
# declare alpha-unknown-linux-gnu as alternate platform, because some projects
# offer applications for it.
ifeq ($(DEB_HOST_GNU_TYPE), alpha-linux-gnu)
  TYPE_FLAGS += --with-boinc-platform=alpha-hp-linux-gnu \
    --with-boinc-alt-platform=alpha-unknown-linux-gnu
endif

# Change BOINC platform i486-pc-linux-gnu to i686-pc-linux-gnu.
ifeq ($(DEB_HOST_GNU_TYPE), i486-linux-gnu)
  TYPE_FLAGS += --with-boinc-platform=i686-pc-linux-gnu
endif

# Change BOINC platform x86_64-linux-gnu to x86_64-pc-linux-gnu.
# Keeping the previous name temporarily.
# The 32bit variants shall allow the execution of 32 bit
# executable in case the scientific application is no available
# for the complete bit size.
ifeq ($(DEB_HOST_GNU_TYPE), x86_64-linux-gnu)
  TYPE_FLAGS += --with-boinc-platform=x86_64-pc-linux-gnu
  # wrong previous name (since regular Debian name)
  TYPE_FLAGS += --with-boinc-alt-platform=x86_64-linux-gnu
  # if we did the following, the the ia32 libs would
  # need to become a dependency - which is not wanted by too many
  #TYPE_FLAGS += --with-boinc-alt-platform=i686-pc-linux-gnu
endif

# Change BOINC platform i486-pc-kfreebsd-gnu to i686-pc-kfreebsd-gnu to be
# consistent with the official BOINC platform i686-pc-linux-gnu.
ifeq ($(DEB_HOST_GNU_TYPE), i486-kfreebsd-gnu)
  TYPE_FLAGS += --with-boinc-platform=i686-pc-kfreebsd-gnu
endif

# Declare i686-pc-kfreebsd-gnu as alternate platform for
# x86_64-pc-kfreebsd-gnu.
ifeq ($(DEB_HOST_GNU_TYPE), x86_64-kfreebsd-gnu)
  TYPE_FLAGS += --with-boinc-platform=x86_64-pc-kfreebsd-gnu
  TYPE_FLAGS += --with-boinc-alt-platform=i686-pc-kfreebsd-gnu
endif

# Change BOINC platform ia64-unknown-linux-gnu to ia64-linux-gnu.
ifeq ($(DEB_HOST_GNU_TYPE), ia64-linux-gnu)
  TYPE_FLAGS += --with-boinc-platform=ia64-linux-gnu
endif

# Change BOINC platform powerpc-unknown-linux-gnu to powerpc-linux-gnu.
ifeq ($(DEB_HOST_GNU_TYPE), powerpc-linux-gnu)
  TYPE_FLAGS += --with-boinc-platform=powerpc-linux-gnu
endif

# Change BOINC platform powerpc64-unknown-linux-gnu to ppc64-linux-gnu and
# declare powerpc-linux-gnu as alternate platform.
ifeq ($(DEB_HOST_GNU_TYPE), powerpc64-linux-gnu)
  TYPE_FLAGS += --with-boinc-platform=ppc64-linux-gnu \
    --with-boinc-alt-platform=powerpc-linux-gnu
endif

# Change BOINC platform sparc-unknown-linux-gnu to sparc-sun-linux-gnu.
ifeq ($(DEB_HOST_GNU_TYPE), sparc-linux-gnu)
  TYPE_FLAGS += --with-boinc-platform=sparc-sun-linux-gnu
endif

# Not touching BOINC platform for hurd for the very moment.
# Manually specify the MAXPATHLEN for GNU where this limit
# is not existing.
ifeq ($(DEB_HOST_GNU_TYPE), i486-gnu)
  # setting it to the regular limit for the other flavours of Debian
  CFLAGS += -DMAXPATHLEN=4096
  #TYPE_FLAGS += --with-boinc-platform=i686-pc-hurd-gnu
endif

ifneq (,$(findstring clang,$(DEB_BUILD_OPTIONS)))
  export CXX=clang
endif

CFLAGS += -Wall
CXXFLAGS += -Wall

ifeq (,$(DEB_OPT_FLAGS))
   DEB_OPT_FLAGS = -O3 -funroll-loops -fforce-addr -ffast-math
   $(info W: No default DEB_OPT_FLAGS set, adding $(DEB_OPT_FLAGS))
endif

CFLAGS += $(DEB_OPT_FLAGS)
CXXFLAGS += $(DEB_OPT_FLAGS)

CFLAGS_boinc-client := $(CFLAGS)
CXXFLAGS_boinc-client := $(CXXFLAGS)

CCorig := $(CC)
CXXorig := $(CXX)

# Link-time optimisation demands knowledge about all CFLAGS both at
# compile time (as represented by CFLAGS and CXXFLAGS) but also at
# at link time.
ifeq (,$(findstring nolto,$(DEB_BUILD_OPTIONS)))
 $(info I: Activating LinkTimeOptimisation)
 CFLAGS += -flto
 CXXFLAGS += -flto
 LDFLAGS += -flto
 CXX := $(CXX) $(CXXFLAGS)
 CC := $(CC) $(CFLAGS)
 AR := gcc-ar
 RANLIB := gcc-ranlib
endif

#copy the original control.in file
$(shell cp debian/control.in debian/control)
$(shell sed '/^#/d' -i debian/control)

export ENABLE_SERVER_TEST := $(shell dpkg-parsechangelog -S Distribution |egrep -v 'experimental|UNRELEASED')

ifeq (,$(ENABLE_SERVER_TEST))
$(shell sed 's/^%//' -i debian/control)
else
$(shell sed '/^%/d' -i debian/control)
endif

ifeq (,$(ENABLE_SERVER_TEST))
$(info I: Building both the server and the client packages)
ENABLE_SERVER := --enable-server
else
$(info I: Building regular client packages only)
#ENABLE_SERVER := --disable-server
endif

#ENABLE_CLIENT_TEST := $(shell egrep '^Package:.*boinc$$' debian/control debian/control.in)
#ifneq (,$(ENABLE_CLIENT_TEST))
ENABLE_CLIENT := --enable-client --enable-manager --enable-dynamic-client-linkage
$(shell sed 's/^@//' -i debian/control)
#else
#ENABLE_CLIENT := --disable-client --disable-manager
#$(shell sed '/^@/d' -i debian/control)
#endif

# --disable-static
# For debugging, these options are useful:
#   --enable-debug
#   --enable-wx-debug (need to install libwxbase3.0-0-dbg and libwxgtk3.0-0-dbg)
#
CFGFLAGS_boinc-client = \
  $(TYPE_FLAGS) \
  --prefix=/usr \
  --bindir=/usr/bin \
  --localstatedir=/var/lib \
  $(ENABLE_CLIENT) \
  $(ENABLE_SERVER) \
  --disable-fcgi \
  --enable-libraries \
  --enable-unicode \
  --enable-shared \
  --enable-static \
  --enable-dynamic-client-linkage \
  --with-ssl \
  --with-x \
  CFLAGS="$(CFLAGS_boinc-client)"

%:
	dh $@ --with python3 --no-parallel

override_dh_autoreconf-indep:

override_dh_clean:
	dh_clean
	rm -rf config.log
	rm -rf clientscr/.libs
	rm -rf debian/boinc-app-examples   debian/boinc-app-examples.substvars
	rm -rf debian/boinc-server-maker   debian/boinc-server-maker.substvars
	rm -rf debian/boinc-cgi-stripchart debian/boinc-cgi-stripchart.substvars
	rm -rf debian/boinc-dbg            debian/boinc-dbg.substvars
	rm -rf debian/boinc-server-dbg     debian/boinc-server-dbg.substvars
	rm -rf debian/*.debhelper
	rm -rf debian/tmp autoreconf-stamp build-*-stamp install-*-stamp
	rm -fr debian/boinc-amd-opencl debian/boinc-screensaver # for cases when this is not in debian/control
	rm -f debian/boinc-screensaver.substvars debian/boinc-amd-opencl.substvars # dito
	rm -f debian/boinc-cgi-stripchart.substvars
	# sudden differences yet remaining to be resolved
	rm -f api/libboinc_graphics2.la
	rm -f test-driver
	rm -f sched/adjust_user_priority sched/size_regulator
	rm -f config.status libtool version.h stamp-h1
	find . -name ".libs" | xargs -r rm -r
	find zip -name "*.lo" -delete
	rm -rf py/lib
	if [ -d .git ]; then \
	  if [ -x /usr/bin/git ]; then \
	    git diff debian/control | patch -p1 -R; \
	  fi ; \
	fi

# Commands not to execute - empty overrides
override_dh_auto_test:

#test-stamp:
#	dh_testdir
#	xmllint --nonet --noout \
#	  debian/conffiles/cc_config.xml \
#	  debian/conffiles/global_prefs_override.xml
#	touch $@
LDFLAGS+=$(CFLAGS)

override_dh_auto_configure-indep:

override_dh_auto_configure-arch:
	debconf-updatepo
	#./_autosetup
	#if [ ! -r ltmain.sh ]; then libtoolize; fi
	#aclocal -I m4 && autoheader && automake --add-missing && autoconf
	@echo
	@echo "****             *****"
	@echo "**** Configuring *****"
	@echo "****             *****"
	@echo "**** CXX=$(CXX)"
	@echo "**** LD=$(LD)"
	@echo "**** AR=$(AR)"
	@echo "**** RANLIB=$(RANLIB)"
	@echo "****             *****"
	@echo
	dh_auto_configure -- $(CFGFLAGS_boinc-client)

update-boinc-applinks.1: debian/manpages/update-boinc-applinks.xml
	docbook2x-man debian/manpages/update-boinc-applinks.xml

# noopt, not necessary
# CFLAGS_noopt = $(subst -O2,-O0,$(CFLAGS))
# CXXFLAGS_noopt = $(subst -O2,-O0,$(CXXFLAGS))
# noopt and no hardening
CFLAGS_plain = -g -O0 -Wall
CXXFLAGS_plain = $(CFLAGS_plain)

override_dh_auto_build-indep:

override_dh_auto_build-arch: update-boinc-applinks.1
	@echo
	@echo "****            *****"
	@echo "**** Making     *****"
	@echo "****            *****"
	@echo "**** CC=$(CC)"
	@echo "**** CXX=$(CXX)"
	@echo "**** AR=$(AR)"
	@echo "**** RANLIB=$(RANLIB)"
	@echo
	# disable optimization in api/, for SETI to work
	#$(MAKE) -C api CFLAGS="$(CFLAGS_noopt)" CXXFLAGS="$(CXXFLAGS_noopt)"

# we need to build zip because of the libboinc7 reference, until we split the dev packages or we move it in a new package
#ifeq (,$(ENABLE_SERVER_TEST))
	# TODO For some reasons unzip build fails if hardening flags are enabled.
	$(MAKE) CC=$(CCorig) CXX=$(CXXorig) -C zip CFLAGS="$(CFLAGS_plain) -fPIC" CXXFLAGS="$(CXXFLAGS_plain)"
	$(MAKE) CC=$(CCorig) CXX=$(CXXorig) -C zip/unzip CFLAGS="$(CFLAGS_plain) -fPIC" CXXFLAGS="$(CXXFLAGS_plain)"
#endif

	# Generate binary message catalogs of the BOINC Manager.
#	if [ -d locale ]; then \
#	  for i in `ls locale`; do \
#	    if [ -f "locale/$$i/BOINC-Manager.po" ]; then \
#	     msgfmt -o "locale/$$i/BOINC-Manager.mo" \
#	      "locale/$$i/BOINC-Manager.po"; \
#	    fi; \
#	  done; \
#	fi
	dh_auto_build
ifeq (,$(ENABLE_SERVER_TEST))
	$(MAKE) -C samples/sleeper
	$(MAKE) -C samples/worker
	$(MAKE) -C samples/example_app
	$(MAKE) -C samples/wrapper
endif

BINARIES= sched/census sched/cgi sched/db_dump sched/db_purge \
	sched/delete_file sched/feeder sched/file_deleter \
	sched/file_upload_handler sched/get_file sched/make_work \
	sched/message_handler sched/request_file_list sched/sample_assimilator \
	sched/sample_bitwise_validator sched/sample_dummy_assimilator \
	sched/sample_trivial_validator sched/sample_work_generator \
	sched/sched_driver sched/send_file sched/show_shmem \
	sched/single_job_assimilator sched/transitioner sched/trickle_handler \
	sched/update_stats sched/wu_check \
	clientgui/boincmgr
BINARIES += samples/worker/worker samples/wrapper/wrapper samples/sleeper/sleeper \
	samples/nvcuda/cuda samples/example_app/example_app samples/vm_wrapper/vm_wrapper \
	samples/atiopencl/atiopencl

override_dh_auto_clean:
	dh_auto_clean
	[ -r samples/sleeper/Makefile     ] && $(MAKE) -C samples/sleeper     clean
	[ -r samples/worker/Makefile      ] && $(MAKE) -C samples/worker      clean
	[ -r samples/example_app/Makefile ] && $(MAKE) -C samples/example_app clean
	[ -r samples/wrapper/Makefile     ] && $(MAKE) -C samples/wrapper     clean
	find . -name Makefile.in -o -name "*.a" -o -name "*.o" | xargs -r rm
	find . -name "Makefile" | grep -v samples | xargs -r rm
	rm -f configure clientscr/boincscr
	rm -rf py/lib.linux-*-2.*
	rm -f client_state.xml gui_rpc_auth.cfg
	rm -rf  py/build py/lib.linux-x86_64-2.6
	find . -name "*.pyc" | xargs -r rm
	rm -f stderr.txt # remnant of application started
	rm -f lib/crypt_prog test/run notices/feeds.xml
	find test -name "run-*" | xargs -r rm -rf
	rm -f $(BINARIES)
	rm -f \
	  boinc_lockfile lockfile \
	  client/boinc.optimized \
	  update-boinc-applinks.1 \
	  aclocal.m4 \
	  config.h \
	  config.h.in \
	  configure \
	  compile \
	  depcomp \
	  doc/manpages/appmgr.8 \
	  install-sh \
	  ltmain.sh \
	  m4/libtool.m4 \
	  m4/ltoptions.m4 \
	  m4/ltsugar.m4 \
	  m4/ltversion.m4 \
	  m4/lt~obsolete.m4 \
	  missing \
	  sched/credit_test \
	  svn_version.h \
	  config.guess \
	  config.sub \
	  client/scripts/Makefile \
	  client/scripts/boinc-client \
	  clientgui/res/Makefile \
	  sea/Makefile   \
	  client/boinc \
	  packages/solaris/CSW/boincclient/pkginfo \
	  packages/solaris/CSW/boincclient/prototype \
	  packages/solaris/CSW/boincdevel/pkginfo \
	  packages/solaris/CSW/boincdevel/prototype \
	  packages/solaris/CSW/boinclibs/pkginfo \
	  packages/solaris/CSW/boinclibs/prototype \
	  packages/solaris/CSW/boincmanager/pkginfo \
	  packages/solaris/CSW/boincmanager/prototype \
	  packages/solaris/CSW/Makefile \
	  packages/generic/sea/Makefile \
	  packages/solaris/CSW/boincclient/Makefile \
	  packages/solaris/CSW/boincdevel/Makefile \
	  packages/solaris/CSW/boinclibs/Makefile \
	  packages/solaris/CSW/boincmanager/Makefile

SAMPLES=samples/condor/boinc_gahp samples/example_app/uc2 samples/multi_thread/multi_thread samples/sleeper/sleeper samples/sporadic/sporadic samples/worker/worker samples/wrapper/wrapper

override_dh_auto_install-indep:

override_dh_auto_install-arch: update-boinc-applinks.1
	dh_auto_install
ifeq (,$(ENABLE_SERVER_TEST))
	echo "I: Installing example scientific applications"
	mkdir -p $(CURDIR)/debian/tmp/samples
	cp $(SAMPLES) $(CURDIR)/debian/tmp/samples || echo "W: Could not install samples ($(SAMPLES))"
	cd $(CURDIR)/debian/tmp/usr/share/boinc-server-maker/html/drupal/sites/all/libraries/fonts/ && \
	for i in `ls *.ttf`; do \
		rm -r "$$i" ; \
		ln -sf "/usr/share/fonts/truetype/dejavu/$$i" . ; \
	done;
endif
	for i in $(CURDIR)/debian/*/usr/bin ; \
	do \
		egrep -l "^#!.*env..*python" $$i/* | xargs -r sed -i '1s%#!.*%#!/usr/bin/python3%' ; \
		egrep -l "^#!/usr/bin/python2" $$i/* | xargs -r sed -i '1s%#!.*%#!/usr/bin/python3%' ; \
	done

override_dh_install:
	# boinc apps are built only together with the server
	rm -rf debian/boinc-manager/usr/share/locale/en_US/
	# delete empty dirs
	if [ -d debian/boinc-manager/usr/share/locale ] ; then find debian/boinc-manager/usr/share/locale -type d -empty -delete; fi
	# delete .la files (Closes: #621197)
	find $(CURDIR)/debian -name "*.la" | xargs -r rm -f
	dh_install
	find debian/boinc-server-maker/usr/share/ -name sleeper -type f -delete || true
	find debian/boinc-server-maker/usr/share/ -name wrapper -type f -delete || true
	find debian/boinc-server-maker/usr/share/ -name worker -type f -delete || true
	find debian/boinc-server-maker/usr/share/ -name .gitignore -o -name .cvsignore -delete || true

override_dh_missing:
	dh_missing --list-missing

override_dh_fixperms:
	dh_fixperms
	# some files are not right with 755
	find debian -name "*.php" -o -name "*.php.example" -o -name "*.cpp" | grep -v "update_translations.php" | grep -v "transitioner_catchup.php" | xargs -L 1 -r chmod 644 --
	# some other files are indeed to be executed via the command line
	opsdir=debian/boinc-server-maker/usr/share/boinc-server-maker/html/ops ; \
	if [ -d "$$opsdir" ]; then \
		cd "$$opsdir" ; \
		for i in *.php; do \
			if head -n 1 "$$i" | grep -q "^#!"; then \
				chmod 755 "$$i" ; \
			fi ; \
		done ; \
	fi
	# Correct catalog file perms
	find debian -name "*.mo" -exec chmod 644   {} \;
	[ -d debian/boinc-client ] && chmod 755 \
	  debian/boinc-client/usr/share/boinc-client/udev-cpu_share \
	  debian/boinc-client/usr/share/bug/boinc-client/script \
	  debian/boinc-manager/usr/share/doc/boinc-manager/examples/run-boincmgr || true
	[ -d debian/boinc-server-maker ] && chmod +x \
	  debian/boinc-server-maker/usr/share/boinc-server-maker/html/ops/delete_job_files || true

override_dh_compress:
	dh_compress -Xsamples -X.cpp -X.py -XMakefile

override_dh_installexamples:
	dh_installexamples -X.gz -Xjpeglib -X.o
